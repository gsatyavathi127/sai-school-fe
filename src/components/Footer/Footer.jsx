import React from "react";
import "./Footer.scss";
import Logo from "../../assets/nav_footer/images/logo.svg";
import Peacock from "../../assets/nav_footer/images/peacock.svg";
import Phone from "../../assets/nav_footer/icons/phone.svg";
import Mail from "../../assets/nav_footer/icons/mail.svg";
import Location from "../../assets/nav_footer/icons/location.svg";
import Whatsapp from "../../assets/nav_footer/icons/whatsapp.svg";
import Facebook from "../../assets/nav_footer/icons/facebook.svg";
import Instagram from "../../assets/nav_footer/icons/instagram.svg";
import Clock from "../../assets/nav_footer/icons/clock.svg";

const Footer = () => {
  return (
    <div className="footer_main_wrapper">
      <img src={Logo} alt="logo" className="logo" />
      <img src={Peacock} alt="peacock" className="peacock" />
      <div className="footer_content">
        <div className="section_1">
          <div className="phone">
            <img src={Phone} alt="" />
            <div>
              <h4>Call</h4>
              <p>9955778822</p>
            </div>
          </div>
          <div className="phone">
            <img src={Mail} alt="" />
            <div>
              <h4>Mail</h4>
              <p>SGHS@gamil.com</p>
            </div>
          </div>
          <div className="phone">
            <img src={Location} alt="" />
            <div>
              <h4>Address</h4>
              <p>
                8-7-12/9/1. Sai Colony,Padmashalipuram, Sai Colony, Telangana
                NGOS Colony, Katedhan, Telangana 500077
              </p>
            </div>
          </div>
        </div>
        <div className="section_2">
          <img src={Whatsapp} alt="" />
          <div>
            <h4>Quick Links</h4>
            <ul>
              <li>
                <a href="/home">Home</a>
              </li>
              <li>
                <a href="/academics">Academics</a>
              </li>
              <li>
                <a href="/school-life">School life</a>
              </li>
              <li>
                <a href="/gallery">Gallery</a>
              </li>
            </ul>
          </div>
        </div>

        <div className="section_2">
          <img src={Clock} alt="" />
          <div>
            <h4>School Timings</h4>
            <ul>
              <li>Nursery & PP I – 8:30 AM to 12:35 PM</li>
              <li>PP II to X – 8:30 AM to 3:00 PM</li>
              <li>Class I to X – Monday to Saturday</li>
            </ul>
            <div className="social_icons">
              <a href="/">
                <img src={Instagram} alt="" />
              </a>
              <a href="/">
                <img src={Facebook} alt="" />
              </a>
              <a href="/">
                <img src={Whatsapp} alt="" />{" "}
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
