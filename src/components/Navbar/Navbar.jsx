import React from "react";
import "./Navbar.scss";
import Logo from "../../assets/nav_footer/images/logo.svg";
import Phone from "../../assets/nav_footer/icons/phone_nav.svg";

const Navbar = () => {
  return (
    <div className="navbar_main_wrapper">
      <div className="logo">
        <a href="/home">
          <img src={Logo} alt="school_logo" />
        </a>
      </div>
      <div className="right_nav">
        <div className="top_nav">
          <p>
            <img src={Phone} alt="school_logo" />
            <span>9955778822</span>
          </p>
          <span className="adm_open">Admissions Open</span>
        </div>
        <div className="routes">
          <ul>
            <li>
              <a href="/home">Home</a>
            </li>
            <li>
              <a href="/academics">Academics</a>
            </li>
            <li>
              <a href="/beyond-academics">Beyond Academics</a>
            </li>
            <li>
              <a href="/school-life">School life</a>
            </li>
            <li>
              <a href="/gallery">Gallery</a>
            </li>
            <li>
              <a href="/about-us">About Us</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
