import React from "react";
import "./BeyondAcademics.scss";
import BeyondAcademicHero from "../../assets/beyondAcademics/beyond_aca_hero.svg";
import Meditation from "../../assets/beyondAcademics/meditation.svg";
import Arts from "../../assets/beyondAcademics/arts.svg";
import Wheel from "../../assets/beyondAcademics/art_fan.svg";
import Cultutal from "../../assets/beyondAcademics/cultural.svg";
import pinWheel from "../../assets/pinWheel.json";

import Lottie from "lottie-react";

const BeyondAcademics = () => {
  return (
    <div className="beyond_academics_main_wrapper">
      <div className="hero_section">
        <img src={BeyondAcademicHero} alt="hero" />
      </div>
      <div className="main_section">
        <div className="sub_section">
          <div className="content">
            <h3>Meditation</h3>
            <p>
               The sports curriculum at SGHS is planned and designed for
              students to develop and enhance skills such as strength, speed,
              endurance, agility, flexibility, control, balance, as well as
              team-work, leadership, confidence and self-discipline.
            </p>
          </div>
          <img src={Meditation} alt="" />
        </div>

        <div className="sub_section arts_section">
          <img src={Arts} alt="" />
          <div className="content">
            <Lottie
              animationData={pinWheel}
              loop={true}
              autoplay={true}
              className="lottie"
            />
            <img src={Wheel} alt="wheel" className="wheel" />
            <h3>Arts & Crafts</h3>
            <p>
              SCHS culture has always valued participation and teamwork and lays
              a strong emphasis on co-curricular activities. The school was
              founded on the principle of imparting holistic education with an
              equal emphasis on arts, sports and academics. The Physical
              Education Curriculum at school provides the right balance of
              activities to develop children’s gross and fine motor skills.
            </p>
          </div>
        </div>
        <div className="sub_section">
          <div className="content">
            <h3>Physical Education</h3>
            <p>
               The sports curriculum at SGHS is planned and designed for
              students to develop and enhance skills such as strength, speed,
              endurance, agility, flexibility, control, balance, as well as
              team-work, leadership, confidence and self-discipline.
            </p>
          </div>
          <img src={Meditation} alt="" />
        </div>
        <div className="sub_section  cultural_Section">
          <img src={Cultutal} alt="" />
          <div className="content">
            <h3>Cultural Activities</h3>
            <p>
               The sports curriculum at SGHS is planned and designed for
              students to develop and enhance skills such as strength, speed,
              endurance, agility, flexibility, control, balance, as well as
              team-work, leadership, confidence and self-discipline.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BeyondAcademics;
