import React, { useState } from "react";
import "./Home.scss";
import Slide1 from "../../assets/home/slide1.svg";
import Slide2 from "../../assets/home/slide2.svg";
import SGHS from "../../assets/home/sghs.svg";
import Rainbow from "../../assets/home/rainbow.png";
import Knowmore from "../../assets/home/know_more.svg";
import NoticeBoardIcon from "../../assets/home/notice_board_icon.svg";
import NoteIcon from "../../assets/home/note_icon.svg";
import ClassRoom from "../../assets/home/classroom.png";

import { Swiper, SwiperSlide } from "swiper/react";
import {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Autoplay,
} from "swiper/modules";

import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

const noticeBoardContent = [
  {
    title: "Notice One",
    content: "Annual day will be on 30/11/2023",
  },
  {
    title: "Notice One",
    content: "Annual day will be on 30/11/2023",
  },
  {
    title: "Notice One",
    content: "Annual day will be on 30/11/2023",
  },
];

const Home = () => {
  const [noticeBoard, setNoticeBoard] = useState(noticeBoardContent);

  const params = {
    autoplay: {
      delay: 2000,
      disableOnInteraction: false,
    },
  };

  return (
    <div className="home_main_wrapper">
      <div className="hero_slider_wrapper">
        <Swiper
          modules={[Navigation, Pagination, Scrollbar, A11y, Autoplay]}
          spaceBetween={0}
          slidesPerView={1}
          navigation
          loop={true}
          pagination={{ clickable: true }}
          speed={2000}
          {...params}
        >
          <SwiperSlide className="slide1">
            <img src={Slide1} alt="" />
          </SwiperSlide>
          <SwiperSlide className="slide2">
            <img src={Slide2} alt="" />
          </SwiperSlide>
        </Swiper>
      </div>
      <div className="section_2">
        <div className="left">
          <p>Welcome To</p>
          <h3>
            SAI GRAMMAR HIGH <br /> SCHOOL
          </h3>
          <div className="content">
            Established in 2002 , SAI GRAMMAR HIGH SCHOOL was founded as a
            vibrant summer camp for children to develop their skills in arts,
            sports, and technology. Over the last 20 years, SAI GRAMMAR HIGH
            SCHOOL International School has grown to become a leader in K-12
            education as an institution that instills a passion for learning and
            innovation. SAI GRAMMAR HIGH SCHOOL offers CBSE, IB and Cambridge
            curricula, and is recognized as one of the best schools in
            Hyderabad, Telangana and in India. The institution is also a proud
            member of the Council of International Schools (CIS), conferred as a
            result of demonstrating commitment to high quality international
            education.
          </div>
          <a href="/about-us" className="know_more_btn">
            <img src={Knowmore} alt="" />
          </a>
        </div>
        <img src={Rainbow} alt="" className="rainbow_img" />
        <img src={SGHS} alt="" className="sghs_text" />
      </div>
      <div className="section_3">
        <img src={NoticeBoardIcon} alt="" className="notice_board_icon" />
        <img src={NoteIcon} alt="" className="note_icon" />
        <div className="notice_board">
          <ul>
            {noticeBoard.map((item, index) => {
              return (
                <li key={index}>
                  <h4>{item.title}</h4>
                  <p>{item.content}</p>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
      <div className="section_4">
        <h3>Today at SGHS</h3>
        <div className="images">
          <div>
            <img src={ClassRoom} alt="class_room" />
            <span>Class Room</span>
          </div>
          <div>
            <img src={ClassRoom} alt="class_room" />
            <span>Class Room</span>
          </div>
          <div>
            <img src={ClassRoom} alt="class_room" />
            <span>Class Room</span>
          </div>
        </div>
        <a href="/about-us" className="know_more_btn">
          <img src={Knowmore} alt="" />
        </a>
      </div>
    </div>
  );
};

export default Home;
