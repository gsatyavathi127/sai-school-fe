import React, { useEffect, useState } from "react";
import "./Gallery.scss";
import GalleryHero from "../../assets/gallery_hero.png";
import Classroom from "../../assets/home/classroom.png";
import Rainbow from "../../assets/home/rainbow.png";
import { useNavigate } from "react-router-dom";

const galleryData = [
  {
    year: "2023",
    month: "January",
    eventName: "Annual Day",
    images: [Classroom, Rainbow],
  },
  {
    year: "2023",
    month: "January",
    eventName: "Farewell Day",
    images: [Rainbow, Classroom],
  },
  {
    year: "2022",
    month: "September",
    eventName: "Annual Day",
    images: [Classroom, Classroom],
  },
  {
    year: "2021",
    month: "February",
    eventName: "Annual Day",
    images: [Classroom, Classroom],
  },
  {
    year: "2020",
    month: "January",
    eventName: "Annual Day",
    images: [Classroom, Classroom],
  },
];

const years = () => {
  let yearsArr = [];
  for (let i = 2023; i >= 2015; i--) {
    yearsArr.push(i);
  }
  return yearsArr;
};

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

const Gallery = () => {
  const [filterInputs, setFilterInputs] = useState({
    year: 2023,
    month: "January",
    eventName: "",
  });

  const [data, setData] = useState({});

  const { year, month, eventName } = filterInputs;

  const Navigate = useNavigate();

  const handleFilterChange = (e) => {
    const { name, value } = e.target;
    setFilterInputs({ ...filterInputs, [name]: value });
  };

  useEffect(() => {
    const filteredData = galleryData
      .filter((item) => item.year == year)
      .filter((item) => item.month === month)
      .filter((item) => {
        return item.eventName.toLowerCase().includes(eventName.toLowerCase());
      });

    setData(filteredData);
  }, [filterInputs]);

  // console.log(data, filterInputs);
  return (
    <div className="gallery_main_wrapper">
      <div className="gallery_hero_wrapper">
        <img src={GalleryHero} alt="" />
      </div>
      <div className="gallery_search_main_wrapper">
        <h3>Gallery Search</h3>
        <div className="filters">
          <label htmlFor="year">Year*</label>
          <select name="year" id="year" onChange={handleFilterChange}>
            {years().map((year, index) => {
              return (
                <option value={year} key={index}>
                  {year}
                </option>
              );
            })}
          </select>
          <label htmlFor="month">Month*</label>
          <select name="month" id="month" onChange={handleFilterChange}>
            {months.map((month, index) => {
              return (
                <option value={month} key={index}>
                  {month}
                </option>
              );
            })}
          </select>
          <label htmlFor="month">Event Name</label>
          <input
            type="text"
            name="eventName"
            value={eventName}
            onChange={handleFilterChange}
          />
        </div>

        {data?.length > 0 ? (
          <div className="">
            <h3>year {data[0].year}</h3>
            {data.map((item, index) => {
              return (
                <div key={index} className="gallery_images">
                  {item.images.map((image) => {
                    return (
                      <img
                        src={image}
                        alt=""
                        onClick={() => Navigate("/image-gallery")}
                      />
                    );
                  })}
                </div>
              );
            })}
          </div>
        ) : (
          <h4>No images found</h4>
        )}
      </div>
    </div>
  );
};

export default Gallery;
