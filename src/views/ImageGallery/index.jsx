import React, { useState } from "react";
import "./ImageGallery.scss";
import Classroom from "../../assets/home/classroom.png";
import Rainbow from "../../assets/home/rainbow.png";

import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper/modules";

import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

const imageGalleryData = [Rainbow, Classroom];

const ImageGallery = () => {
  const [selectedImage, setSelectedImage] = useState(imageGalleryData[0]);

  const handleClickImage = (item) => {
    setSelectedImage(item);
  };

  return (
    <div className="image_gallery_main_wrapper">
      <div className="selected_image_wrapper">
        <img src={selectedImage} alt="" />
      </div>
      <div className="images">
        <Swiper
          modules={[Navigation, Pagination, Scrollbar, A11y]}
          spaceBetween={0}
          slidesPerView={2}
          navigation
          loop={true}
          speed={500}
          onSlideChange={() => console.log("slide change")}
          onSwiper={(swiper) => console.log(swiper, "Swipwee")}
        >
          {imageGalleryData.map((item) => {
            return (
              <SwiperSlide className="slide1">
                <img
                  src={item}
                  className={`${item === selectedImage && "active_image"}`}
                  alt=""
                  onClick={() => handleClickImage(item)}
                />
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>
    </div>
  );
};

export default ImageGallery;
