import React from "react";
import { Routes, Route, BrowserRouter } from "react-router-dom";

import RootLayout from "./Layout/RootLayout";
import Home from "./views/Home";
import Academics from "./views/Academics";
import BeyondAcademics from "./views/BeyondAcademics";
import SchoolLife from "./views/SchoolLife";
import AboutUs from "./views/AboutUs";
import Gallery from "./views/Gallery";
import ImageGallery from "./views/ImageGallery";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/" element={<RootLayout />}>
            <Route index element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="/academics" element={<Academics />} />
            <Route path="/beyond-academics" element={<BeyondAcademics />} />
            <Route path="/school-life" element={<SchoolLife />} />
            <Route path="/gallery" element={<Gallery />} />
            <Route path="/about-us" element={<AboutUs />} />
            <Route path="/image-gallery" element={<ImageGallery />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
